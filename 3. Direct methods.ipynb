{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 103,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from functools import reduce\n",
    "from timeit import Timer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can list three main methods for direct methods; \n",
    "\n",
    "| Method                   | Initial form | Final form  |   |   |\n",
    "|--------------------------|--------------|-------------|---|---|\n",
    "| Gauss-Elimination        | Ax = b       | Ux = c      |   |   |\n",
    "| LU Decomposition         | Ax = b       | LUx = b     |   |   |\n",
    "| Gauss-Jordan Elimination | Ax = b       | Ix = c      |   |   |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Square matrix is triangular if it contains only zero elements on one side of the leading diagonal, therefore;\n",
    "\n",
    "* U stands for upper triangular matrix \n",
    "\\begin{equation}\n",
    "U=\n",
    "\\begin{bmatrix}\n",
    "{U}_{11} & {U}_{12} & {U}_{13} \\\\\n",
    "0 & {U}_{22} & {U}_{23} \\\\\n",
    "0 & 0 & {U}_{33}\n",
    "\\end{bmatrix}\n",
    "\\end{equation}\n",
    "\n",
    "* L stands for lower triangular matrix \n",
    "\\begin{equation}\n",
    "L=\n",
    "\\begin{bmatrix}\n",
    "{L}_{11} & 0 & 0 \\\\\n",
    "{L}_{21} & {L}_{22} & 0 \\\\\n",
    "{L}_{31} & {L}_{32} & {L}_{33} \\\\\n",
    "\\end{bmatrix}\n",
    "\\end{equation}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "<font color = 'steelblue'><b>Example 1;</font>\n",
    "Check if matrix is singular \n",
    "\n",
    "\\begin{equation}\n",
    "A=\n",
    "\\begin{bmatrix}\n",
    "2.1 & -0.6 & 1.1 \\\\\n",
    "3.2 & 4.7 & -0.8 \\\\ \n",
    "3.1 & -6.5 & 4.1 \n",
    "\\end{bmatrix}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Determinant is: 0.0\n"
     ]
    }
   ],
   "source": [
    "A = np.array([[2.1, -0.6, 1.1], \n",
    "              [3.2, 4.7, -0.8], \n",
    "              [3.1, -6.5, 4.1]])\n",
    "\n",
    "detA = np.linalg.det(A)\n",
    "print('Determinant is:', round(detA,4))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "Matrix is singular, it can be seen from the following linear dependency; \n",
    "\\begin{equation}\n",
    "{r}_{3} = 3{r}_{1}-{r}_{2}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def singular(A):\n",
    "    '''Checks if given matrix is singular.\n",
    "    [IN]: 2D array with corresponding coefficients'''\n",
    "    \n",
    "    det = round(np.linalg.det(A), 4) \n",
    "    \n",
    "    if det < 0.0001:\n",
    "        print('Matrix is singular!, det(A) = {}'.format(det))\n",
    "    else:\n",
    "        print('Matrix is non singular, det(A) = {}'.format(det))\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Gaussian Elimination (GE)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It consists of the following steps:\n",
    "* elimination phase, which transforms equations into form <font><b>Ux = c</font>\n",
    "* back substitution phase \n",
    "\n",
    "Symbolic representation of elimination phase using only elementary operations can be as follows, where equation being subtracted is reffered to <font><b>pivot equation</font>:\n",
    "    \n",
    "\\begin{equation}\n",
    "Eq. (i) \\leftarrow  Eq.(i) - \\lambda * Eq.(j)\n",
    "\\end{equation}\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color = 'steelblue'><b>Example 2:</font>\n",
    "\\begin{equation}\n",
    "4x_{1} - 2x_{2}+x_{3}=11  \\end{equation} \n",
    "\\begin{equation}\n",
    "-2x_{1} + 4x_{2}-2x_{3}=-16 \\end{equation}\n",
    "\\begin{equation}\n",
    "x_{1}-2x_{2} + 4x_{3} = 17\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Matrix is non singular, det(A) = 36.0\n"
     ]
    }
   ],
   "source": [
    "A = [[4, -2, 1],\n",
    "     [-2, 4, -2],\n",
    "     [1, -2, 4]]\n",
    "\n",
    "singular(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Step 1 \n",
    "* Choose Eq.(1) to be pivotting equation\n",
    "* Rewrite equations in augumented matrix form \n",
    "\\begin{bmatrix}\n",
    "4 & -2 & 1 & | & 11 \\\\ \n",
    "-2 & 4 & -2 & | & -16  \\\\\n",
    "1 & -2 & 4 & | & 17  \\\\\n",
    "\\end{bmatrix}\n",
    "* Pick multipliers to eliminate x1 from Eq.(2) and Eq.(3)\n",
    "\n",
    "\\begin{equation}\n",
    "Eq(2) \\leftarrow Eq(2) - (-0.5)*Eq(1) \\end{equation}\n",
    "\n",
    "\\begin{bmatrix}\n",
    "4 & -2 & 1 & | & 11 \\\\ \n",
    "{\\color{Red}0} & 3 & -1.5 & | & -10.50  \\\\\n",
    "{\\color{Red}0} & -1.5 & 3.75 & | & 14.25  \\\\\n",
    "\\end{bmatrix}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Step 2\n",
    "* Now Eq.(2) is the pivotting equation\n",
    "* Pick multipliers to eliminate x2 from Eq.(3)\n",
    "\n",
    "\\begin{equation}\n",
    "Eq(3) \\leftarrow Eq(3) - (-0.5)*Eq(2) \\end{equation}\n",
    "\n",
    "\\begin{bmatrix}\n",
    "4 & -2 & 1 & | & 11 \\\\ \n",
    "{\\color{Red}0} & 3 & -1.5 & | & -10.50  \\\\\n",
    "{\\color{Red}0} & {\\color{Red}0} & 3 & | & 9  \\\\\n",
    "\\end{bmatrix}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The determinant of the matrix should not be affected:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 88,
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'singular' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-88-f454dfbce37f>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m()\u001b[0m\n\u001b[0;32m      3\u001b[0m      [0,0,3]]\n\u001b[0;32m      4\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m----> 5\u001b[1;33m \u001b[0msingular\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mA2\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[1;31mNameError\u001b[0m: name 'singular' is not defined"
     ]
    }
   ],
   "source": [
    "A2 = [[4, -2, 1,],\n",
    "     [0, 3, -1.5],\n",
    "     [0,0,3]]\n",
    "\n",
    "singular(A2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Backsubstitution step\n",
    "\\begin{equation}\n",
    "x_{3} = 3\\\\\n",
    "x_{2} = \\frac{-10.5 + 1.5x_{3}}{3} = -2 \\\\\n",
    "x_{1} = \\frac{11+2x_{2} - x_{3}}{4} = 1\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sanity check"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x1 =  [1.]\n",
      "x2 =  [-2.]\n",
      "x3 =  [3.]\n"
     ]
    }
   ],
   "source": [
    "b = [[11],\n",
    "     [-10.50],\n",
    "     [9]]\n",
    "\n",
    "solution = np.linalg.solve(A2, b)\n",
    "\n",
    "for i,x in enumerate(solution):\n",
    "    print('x'+str(i+1)+' = ', solution[i] )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color = 'steelblue'><b>Example 3:</font> Solve the following system of linear equations Ax = b by the Gaussian elimination (GE). Use the performed calculations to calculated det(A)\n",
    "    \n",
    "\\begin{equation}\n",
    "A=\n",
    "\\begin{bmatrix}\n",
    "1 & 1 & 3 \\\\\n",
    "2 & 1 & 3 \\\\\n",
    "3 & 1 & 6 \n",
    "\\end{bmatrix},\n",
    "b = \n",
    "\\begin{bmatrix}\n",
    "0 \\\\\n",
    "1 \\\\\n",
    "3 \\\\\n",
    "\\end{bmatrix}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Step1\n",
    "* Choose Eq.(1) to be pivotting equation\n",
    "* Rewrite equations in augumented matrix form \n",
    "\\begin{bmatrix}\n",
    "1 & 1 & 3 & | & 0 \\\\ \n",
    "2 & 1 & 3 & | & 1  \\\\\n",
    "3 & 1 & 6 & | & 3  \\\\\n",
    "\\end{bmatrix}\n",
    "\n",
    "* Pick multipliers to eliminate x1 from Eq.(2) and Eq.(3)\n",
    "\n",
    "\\begin{equation}\n",
    "Eq.(2) \\leftarrow Eq.(2) - 2*Eq.(1)\n",
    "\\end{equation}\n",
    "\n",
    "\\begin{bmatrix}\n",
    "1 & 1 & 3 & | & 0 \\\\ \n",
    "{\\color{Red}0} & -1 & -3 & | & 1  \\\\\n",
    "{\\color{Red}0} & -2 & -3 & | & 3  \\\\\n",
    "\\end{bmatrix}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Step2\n",
    "* Now Eq.(2) is the pivotting equation\n",
    "* Pick multipliers to eliminate x2 from Eq.(3)\n",
    "\n",
    "\\begin{bmatrix}\n",
    "1 & 1 & 3 & | & 0 \\\\ \n",
    "{\\color{Red}0} & -1 & -3 & | & 1  \\\\\n",
    "{\\color{Red}0} & {\\color{Red}0} & 3 & | & 1  \\\\\n",
    "\\end{bmatrix}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Backsubstitution step\n",
    "\\begin{equation}\n",
    "x_{3} = 0.33 \\\\\n",
    "x_{2} = -(3x_{3} + 1) = -2 \\\\\n",
    "x_{1} = -x_{2}-3x_{3} = 2 - 1 = 1 \n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sanity check"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 89,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x1 =  [1.]\n",
      "x2 =  [-2.]\n",
      "x3 =  [0.33333333]\n"
     ]
    }
   ],
   "source": [
    "A = [[1, 1, 3],\n",
    "     [2, 1, 3],\n",
    "     [3, 1, 6]]\n",
    "\n",
    "b = [[0], [1], [3]]\n",
    "\n",
    "solution = np.linalg.solve(A,b)\n",
    "for i,x in enumerate(solution):\n",
    "    print('x'+str(i+1)+' = ', solution[i] )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Determinant \n",
    "\n",
    "Can be calculated by taking a product of diagonal elements: \n",
    "\n",
    "\\begin{equation}\n",
    "det(A) = 1 * -1 * 3 = -3\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "-3.0"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def det_triangular(a):\n",
    "    ''' Returns determinant of a triangular matrix'''\n",
    "    product = reduce((lambda x, y: x * y), a)\n",
    "    return float(product)\n",
    "\n",
    "det_triangular([1,-1,3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sanity check"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-3.0\n"
     ]
    }
   ],
   "source": [
    "A = [[1,1,3],\n",
    "     [2,1,3],\n",
    "     [3,1,6]]\n",
    "\n",
    "b = [[0], [1], [3]]\n",
    "\n",
    "detA = np.linalg.det(A)\n",
    "print(round(detA, 4))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Comparison "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 102,
   "metadata": {},
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'Timer' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-102-aca491868ad9>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m()\u001b[0m\n\u001b[1;32m----> 1\u001b[1;33m \u001b[0mprint\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;34m'Triangular matrix determinant execution time: '\u001b[0m\u001b[1;33m,\u001b[0m \u001b[1;34m'{:7.5f}'\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mformat\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mmin\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mTimer\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;32mlambda\u001b[0m\u001b[1;33m:\u001b[0m \u001b[0mdet_triangular\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0ma\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mrepeat\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;36m10\u001b[0m\u001b[1;33m,\u001b[0m\u001b[1;36m10\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[0;32m      2\u001b[0m \u001b[0mprint\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;34m'Full coefficient matrix determinant execution time: '\u001b[0m\u001b[1;33m,\u001b[0m \u001b[1;34m'{:5.4f}'\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mformat\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mmin\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mTimer\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;32mlambda\u001b[0m\u001b[1;33m:\u001b[0m \u001b[0mnp\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mlinalg\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mdet\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mA\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mrepeat\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;36m10\u001b[0m\u001b[1;33m,\u001b[0m\u001b[1;36m10\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;31mNameError\u001b[0m: name 'Timer' is not defined"
     ]
    }
   ],
   "source": [
    "print('Triangular matrix determinant execution time: ', '{:7.5f}'.format(min(Timer(lambda: det_triangular(a)).repeat(10,10))))\n",
    "print('Full coefficient matrix determinant execution time: ', '{:5.4f}'.format(min(Timer(lambda: np.linalg.det(A)).repeat(10,10))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The result for triangular matrix is order of magnitude faster. However we did not take into account time required for Gaussian Elimination process. Therefore wrapp GE into algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### GE Algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Concept"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's consider augumented matrix of some linear system during the Gaussian Elimination Process. <br>\n",
    "Let it be n x n matrix, where: <br>\n",
    "\n",
    "* First k rows are already transformed (nonzero elements only at diagonal and above)\n",
    "* Current pivotting equation is underlined row(k) \n",
    "* row(i) is ith row being transformed, therefore Aik is an eliminated element\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\\begin{pmatrix}\n",
    "{A}_{11} & {A}_{12} & {A}_{13} & \\cdots  & {A}_{1k} & \\cdots & {A}_{1j} & \\cdots  & {A}_{1n} & | & {b}_{1} \\\\ \n",
    "0& {A}_{22} & {A}_{23} & \\cdots & {A}_{2k} & \\cdots & {A}_{2j} & \\cdots & {A}_{2n} & | & {b}_{2}\\\\ \n",
    "0& 0 & {A}_{33} & \\cdots & {A}_{3k} & \\cdots & {A}_{3j} & \\cdots & {A}_{3n} & | & {b}_{3}\\\\ \n",
    "\\vdots & \\vdots & \\vdots &  & \\vdots &  & \\vdots &  & \\vdots & | & \\vdots\\\\ \n",
    "0& 0 & 0 & \\cdots & {A}_{kk} & \\cdots & {A}_{kj} & \\cdots & {A}_{kn} & | & {b}_{k}\\\\ \\hline \n",
    " \\vdots & \\vdots & \\vdots &  & \\vdots &  & \\vdots &  & \\vdots & | & \\vdots \\\\  \n",
    "0 & 0 & 0 & \\cdots & {A}_{ik} & \\cdots & {A}_{ij} & \\cdots & {A}_{in} & | & {b}_{i} \\\\ \n",
    "\\vdots & \\vdots & \\vdots &  & \\vdots &  & \\vdots &  & \\vdots & | & \\vdots \\\\  \n",
    "0 & 0 & 0 & \\cdots & {A}_{nk} & \\cdots & {A}_{nj} & \\cdots & {A}_{nn} & | & {b}_{n} \\\\ \n",
    "\\end{pmatrix}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Elimination step consisted of subtracting from eliminated row pivotting equation multiplied by coefficient lambda"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{equation}\n",
    "row(i) \\leftarrow row(i) - \\lambda * row(pivotting)\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Therefore, lambda should be\n",
    "\n",
    "\\begin{equation}\n",
    "\\lambda = \\frac{A_{ik}}{A_{kk}} \n",
    "\\end{equation}\n",
    "\n",
    "Finally for the following elements in transformed row we have:\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{equation}\n",
    "{A}_{ik} \\leftarrow {A}_{ik} - \\lambda * {A}_{kk}\n",
    "\\end{equation}\n",
    "\\begin{equation}\n",
    "{A}_{ij} \\leftarrow {A}_{ij} - \\lambda * {A}_{kj}\n",
    "\\end{equation}\n",
    "\n",
    "Iterating over elements in transformed row is done by:\n",
    "\\begin{equation}\n",
    "j = k,k+1,n\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Last step is to provide proper iteration over: <br> \n",
    "* rows for picking pivotting row (up to n-1, because what can eliminate the last one?) <br>\n",
    "\\begin{equation}\n",
    "k = 1,2,...,n-1\n",
    "\\end{equation}\n",
    "* for picking eliminated row (next one due to pivott row) <br>\n",
    "\\begin{equation}\n",
    "i = k+1,k+2,...,n\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Implementation - elimination phase"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for k in range(0,n-1): #iterate over all rows but last one for pivotting eq. \n",
    "    \n",
    "    for i in range(k+1, n): #iterate over transformed eq. \n",
    "        \n",
    "        if a[i,k] != 0: #check if eliminated elements is not 0 \n",
    "            \n",
    "            _lambda = a[i,k]/a[k,k] \n",
    "            \n",
    "            a[i,k+1:n] = a[i, k+1:n] - _lambda * a[k, k+1:n] #transform whole row \n",
    "            \n",
    "            b[i] = b[i] - _lambda * b[k]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Above code doesn't replace elements below diagonal with zeros because solution is not using those elements and it's not efficient."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Implementation - back substitution phase"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Last equation is solved by: $\\begin{equation}\n",
    "x_{n} = \\frac{{b}_{n}}{{A}_{nn}}\n",
    "\\end{equation}$. If we consider the stage of back substitution where $\\begin{equation} {x}_{n}, {x}_{n-1},...,{x}_{k+1} \\end{equation}$ have been computed in such order and we are about to evaluate $\\begin{equation} {x}_{k}\\end{equation}$ from $\\begin{equation} k\\end{equation}$th equation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{equation}\n",
    "{A}_{kk}{x}_{k}+{A}_{k,k+1}{x}_{k+1}+...+{A}_{kn}{x}_{n}={b}_{k}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The solution is"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{equation}\n",
    "{x}_{k} = ({b}_{k}-\\sum_{j=k+1}^{n})\\frac{1}{{A}_{kk}},\\quad k=n-1, n-2,...,1\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Therefore algorithm for back substitution is specified as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for k in range(n-1, -1, -1): #going backwards\n",
    "    x[k] = (b[k] - np.dot(a[k, k+1:n],x[k+1:n]))/a[k,k]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Final implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 97,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gaussianElimination(a,b):\n",
    "    '''\n",
    "    Performs gaussian elimination, returns xn, xn-1, xn-2,..., x\n",
    "    '''\n",
    "\n",
    "    n = len(b)\n",
    "    \n",
    "    #Elimination\n",
    "    for k in range(0, n-1): #iterate over all rows but last one for pivotting eq. \n",
    "    \n",
    "        for i in range(k+1, n): #iterate over transformed eq. \n",
    "\n",
    "            if a[i,k] != 0: #check if eliminated elements is not 0 \n",
    "\n",
    "                _lambda = a[i,k]/a[k,k] \n",
    "\n",
    "                a[i,k+1:n] = a[i, k+1:n] - _lambda * a[k, k+1:n] #transform whole row \n",
    "\n",
    "                b[i] = b[i] - _lambda * b[k]\n",
    "                \n",
    "    #Back substitution\n",
    "    for k in range(n-1, -1, -1): #going backwards\n",
    "        b[k] = (b[k] - np.dot(a[k, k+1:n],b[k+1:n]))/a[k,k]\n",
    "    return b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Test\n",
    "* Note this algorithm uses numpy array slicing so it is necesarry to transform input dtype"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 156,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x1 =  [4]\n",
      "x2 =  [-3]\n",
      "x3 =  [0]\n"
     ]
    }
   ],
   "source": [
    "A = np.array([[1,0,1],\n",
    "     [2,3,0],\n",
    "     [-2,-2,1]])\n",
    "\n",
    "b = np.array([[4],[-1],[-2]])\n",
    "\n",
    "solution = gaussianElimination(A,b)\n",
    "\n",
    "for i,x in enumerate(solution):\n",
    "    print('x'+str(i+1)+' = ', solution[i] )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 159,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Gaussian elimination time:  0.00076\n"
     ]
    }
   ],
   "source": [
    "print('Gaussian elimination time: ', '{:7.5f}'.format(min(Timer(lambda: gaussianElimination(A,b)).repeat(10,10))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sanity check"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 142,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x1 =  [4.]\n",
      "x2 =  [-3.]\n",
      "x3 =  [0.]\n"
     ]
    }
   ],
   "source": [
    "A = [[1,0,1],\n",
    "     [2,3,0],\n",
    "     [-2,-2,1]]\n",
    "\n",
    "b = [[4],[-1],[-2]]\n",
    "\n",
    "solution = np.linalg.solve(A,b)\n",
    "for i,x in enumerate(solution):\n",
    "    print('x'+str(i+1)+' = ', solution[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Gaussian Elimination with Partial Pivoting (GEPP)\n",
    "* helps in reducing rounding errors \n",
    "* based on finding a pivot, which is an element with max(abs(val)) in the matrix columns wise \n",
    "* pivot row is not allowed to have zeros\n",
    "* uses rows permutations\n",
    "* computationaly more complex in comparison to GE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Pseudo code\n",
    "\n",
    "* counter = 0 \n",
    "* no_switchs = 0\n",
    "* for column in columns: \n",
    "    * pick_pivot(column)\n",
    "    * pivot eq. = row with pivot\n",
    "    * switch pivot eq. with row[counter] (if necessary), no_switchs ++\n",
    "    * GE\n",
    "    * counter ++ \n",
    "    * check if got triangle matrix\n",
    "        * detA = (-1)^no_switchs * diagonal product\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color = 'steelblue'><b>Example 4:</font> Solve the following system of linear equations Ax=b using GEPP."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{bmatrix}\n",
    "1 & 0 & 1 & | & 4 \\\\ \n",
    "2 & 3 & 0 & | & -1  \\\\\n",
    "-2 & -2 & 1 & | & -2  \\\\\n",
    "\\end{bmatrix}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Check 1st column for pivot element. \n",
    "Since Eq.(2) has 0 it's more convienient to pick -2 and pivot equation to be Eq.(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{bmatrix}\n",
    "{\\color{Orange}1} & 0 & 1 & | & 4 \\\\ \n",
    "{\\color{Orange}2} & 3 & 0 & | & -1  \\\\\n",
    "{\\color{Blue}{-2}} & -2 & 1 & | & -2  \\\\\n",
    "\\end{bmatrix}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Switch pivot eq. with 1st row, switches = 1 \n",
    "\n",
    "\\begin{bmatrix}\n",
    "-2 & -2 & 1 & | & -2 \\\\\n",
    "2 & 3 & 0 & | & -1  \\\\\n",
    "1 & 0 & 1 & | & 4 \\\\\n",
    "\\end{bmatrix}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Eliminate x1 from Eq.(2) and Eq.(3) using current pivot equation  Eq.(1)\n",
    "\n",
    "\\begin{equation}\n",
    "Eq.(2) \\leftarrow Eq.(2) + Eq.(1)\n",
    "\\end{equation}\n",
    "\n",
    "\\begin{bmatrix}\n",
    "-2 & -2 & 1 & | & -2 \\\\\n",
    "{\\color{red}0} & 1 & 1 & | & -3  \\\\\n",
    "1 & 0 & 1 & | & 4 \\\\\n",
    "\\end{bmatrix}\n",
    "\n",
    "\\begin{equation}\n",
    "Eq.(3) \\leftarrow Eq.(3) + 0.5 * Eq.(1)\n",
    "\\end{equation}\n",
    "\n",
    "\\begin{bmatrix}\n",
    "-2 & -2 & 1 & | & -2 \\\\\n",
    "{\\color{red}0} & 1 & 1 & | & -3  \\\\\n",
    "{\\color{red}0} & -1 & 1.5 & | & 3 \\\\\n",
    "\\end{bmatrix}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Check 2nd column for pivot element\n",
    "We can pick either 1 or -1. We pick 1 since it does not require row switching and pivot eq. is Eq.(2)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{bmatrix}\n",
    "-2 & {\\color{orange}{-2}} & 1 & | & -2 \\\\\n",
    "{\\color{red}0} & {\\color{blue}1} & 1 & | & -3  \\\\\n",
    "{\\color{red}0} & {\\color{orange}{-1}} & 1.5 & | & 3 \\\\\n",
    "\\end{bmatrix}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Eliminate x2 from Eq.(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{equation}\n",
    "Eq.(3) \\leftarrow Eq.(3) + Eq.(2)\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{bmatrix}\n",
    "-2 & -2 & 1 & | & -2 \\\\\n",
    "{\\color{red}0} & 1 & 1 & | & -3  \\\\\n",
    "{\\color{red}0} & {\\color{red}{0}} & 2.5 & | & 0 \\\\\n",
    "\\end{bmatrix}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Backsubstitution step"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{equation}\n",
    "x_{3} = 0 \\\\\n",
    "x_{2} = -3 - x_{3} = -3 \\\\\n",
    "x_{1} = 1 + 3 = 4\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sanity check"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "x1 =  [4.]\n",
      "x2 =  [-3.]\n",
      "x3 =  [0.]\n"
     ]
    }
   ],
   "source": [
    "A = [[1,0,1],\n",
    "     [2,3,0],\n",
    "     [-2,-2,1]]\n",
    "\n",
    "b = [[4],[-1],[-2]]\n",
    "\n",
    "solution = np.linalg.solve(A,b)\n",
    "for i,x in enumerate(solution):\n",
    "    print('x'+str(i+1)+' = ', solution[i] )\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Determinant"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "5.0\n"
     ]
    }
   ],
   "source": [
    "switches = 1 \n",
    "det = det_triangular([-2, 1, 2.5])\n",
    "print(det*(-1)**(switches))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Sanity check"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "5.0\n"
     ]
    }
   ],
   "source": [
    "detA = np.linalg.det(A)\n",
    "print(round(detA, 4))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
